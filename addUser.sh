#!/bin/bash

useradd -m -s /bin/bash hacker
passwd hacker

#groups
usermod -G cdrom,floppy,dip,video,netdev,plugdev,scanner,bluetooth,sudo,www-data,adm hacker

#conf sudo

#set locale to en_us.UTF-8
#soit dpkg-reconfigure locales
# soit sed sur /etc/locale.gen puis 
#sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen 
#update-locale LANG=en_US.UTF-8

echo "LC_ALL=en_US.UTF-8" >> /etc/environment
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen 
echo "LANG=en_US.UTF-8" > /etc/locale.conf
locale-gen en_US.UTF-8

echo "127.0.0.1 localhost" > /etc/hosts
echo "127.0.1.1 librehacker" >> /etc/hosts
echo "# The following lines are desirable for IPv6 capable hosts" >> /etc/hosts
echo "::1		localhost ip6-localhost ip6-loopback" >> /etc/hosts
echo "fe00::0		ip6-localnet" >> /etc/hosts
echo "ff00::0		ip6-mcastprefix" >> /etc/hosts
echo "ff02::1		ip6-allnodes" >> /etc/hosts
echo "ff02::2		ip6-allrouters" >> /etc/hosts


echo "librehacker" > /etc/hostname
echo "nameserver 80.67.169.12" > /etc/resolv.conf
echo "nameserver 192.168.1.1" >> /etc/resolv.conf

echo "Password for root"
passwd

#gnome-shell-extension-tool -e
glib-compile-schemas /usr/share/glib-2.0/schemas/

#set autologin for LibreHacker user in /etc/gdm3/daemon.conf
#  AutomaticLoginEnable = true
sed -i 's/#  AutomaticLoginEnable = true/AutomaticLoginEnable = true/g' /etc/gdm3/daemon.conf 

#  AutomaticLogin = hacker
if [ -f /etc/gdm3/daemon.conf ]
then
    sed -i 's/#  AutomaticLogin = user1/AutomaticLogin = hacker/g' /etc/gdm3/daemon.conf
fi
if [ -f /etc/lightdm/lightdm.conf ]
then
    sed -i 's/#autologin-user=/autologin-user=hacker/g'  /etc/lightdm/lightdm.conf
fi

#conf mysql
mysql -u root mysql < /etc/nv_pass.sql
