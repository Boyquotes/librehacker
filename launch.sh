#!/bin/bash

if [ "0$UID" -ne 0 ]; then
   echo "Only root can run $(basename $0) or use $(basename $0)"; exit 1
fi

# tee /tmp/logLH
#umount chroot/dev
#umount chroot/run

###CLEAN
#./clean.sh
#mount /dev/sda1 -o remount,rw,exec,dev,defaults

if [ -e chroot/dev/tty0 ]
then
   echo 'Chroot problem, unmount properly before clean'
   exit 1
fi

lb clean
rm -rf config/

git pull --recurse-submodules

###CONFIG
./lbconfig.sh
./config.sh
#cp custom/hackeros/hackeros.list.chroot config/package-lists/custom.chroot.list

echo "bootstrap" >> /tmp/logue
/usr/bin/lb bootstrap 2>&1 |tee /tmp/logLH

echo "chroot" >> /tmp/logue
/usr/bin/lb chroot 2>&1 |  tee -a /tmp/logLH
echo "chroot done" > chroot/tmp/chroot
#/home/hos5/tuningChroot.sh
#/usr/bin/lb binary 2>&1 | tee -a /tmp/logLH

echo "done" > /tmp/iso
#service apache2 restart

./hackeriser.sh

