#!/bin/bash
source ./variables.sh
lb build 2>&1 | tee logs/log_build-$DISTRIBNAME-$desktop-$time.log


#ISO IS BUILD
echo "End Build Hacker OS $desktop" >> logs/log_build-$DISTRIBNAME-$desktop-$time.log
head -1 logs/log_build-$DISTRIBNAME-$desktop-$time.log | awk {'print $2'}
tail -5 logs/log_build-$DISTRIBNAME-$desktop-$time.log | awk {'print $2'}
mv live-image-amd64.hybrid.iso $DISTRIBNAMEUPPERCASE-$desktop-"$VERSION".iso

ls -l *.iso 

mv $DISTRIBNAMEUPPERCASE-$desktop-"$VERSION".iso /var/www/html/$DISTRIBNAMEUPPERCASE-$desktop-"$VERSION"-"$time".iso 



service apache2 restart
kill $(pgrep mysql)
kill $(pgrep mysqld)
kill $(pgrep mysqld_safe)

service mysql restart

