#!/bin/bash

if [ "0$UID" -ne 0 ]; then
   echo "Only root can run $(basename $0) or use $(basename $0)"; exit 1
fi

if [ ! -e './chroot/usr/bin/apt2hb' ]
then
	echo 'copie'
	cp -ra ./hb-scripts/*  ./chroot/
fi
