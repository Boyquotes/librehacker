#!/bin/bash
source ./variables.sh

#desktopArray=('gnome' 'kde' 'xfce')
#desktopArray=('gnome')
desktopArray=('No desktop')

for desktop in ${desktopArray[*]}
do
	apache_status=`pgrep apache | head -1`
	echo $apache_status
#	if [ -n "$apache_status" ]
#	then
		#service apache2 stop
#	fi

	mysql_status=`pgrep mysql | head -1`
	echo $mysql_status
#	if [ -n "$mysql_status" ]
#	then
		#kill $(pgrep mysql)
		#kill $(pgrep mysqld)
		#kill $(pgrep mysqld_safe)
#	fi


	time=$(date +%Y%m%d-%H%M)

	#archives deb
	#cp -a ./custom/$DISTRIBNAME/archives/* ./config/archives/

	#hooks
	mkdir -p ./config/hooks/normal
	mkdir -p ./config/hooks/live
	cp -ra ./hooks/[a-z]*/* ./config/hooks/normal/
	cp -ra ./hooks/[a-z]*/* ./config/hooks/live/

	#includes.binary
	cp -a ./custom/$DISTRIBNAME/includes.binary/* ./config/includes.binary
	cp ./custom/$DISTRIBNAME/boot/splash.png ./config/includes.binary/isolinux/splash.png

	#includes.chroot
	cp -a ./custom/$DISTRIBNAME/includes.chroot/* ./config/includes.chroot
	cp ./custom/$DISTRIBNAME/background/desktop/background.jpg  ./config/includes.chroot/usr/share/xfce4/backdrops/
	mv ./config/includes.chroot/usr/local/DISTRIBNAME ./config/includes.chroot/usr/local/$DISTRIBNAME

	#replace DISTRIBNAME
	sed -i "s/DISTRIBNAME/$DISTRIBNAME/g" ./config/hooks/*/*.hook.chroot
	sed -i "s/DISTRIBNAME/$DISTRIBNAME/g"  ./config/includes.chroot/etc/skel/.bashrc
	sed -i "s/DISTRIBNAME/$DISTRIBNAME/g"  ./config/includes.chroot/usr/share/glib-2.0/schemas/60*
	sed -i "s/DISTRIBNAMEUPPERCASE/$DISTRIBNAMEUPPERCASE/g" ./config/includes.binary/isolinux/live.cfg

	#replace USER NAME
	sed -i "s/USERCUSTOM/$USERCUSTOM/g" ./config/includes.binary/isolinux/live.cfg
	sed -i "s/USERFULLNAMECUSTOM/$USERFULLNAMECUSTOM/g" ./config/includes.binary/isolinux/live.cfg

	#replace host
	sed -i "s/HOST/$HOST/g" ./config/includes.binary/isolinux/live.cfg

	echo $desktop" INSTALLATION ready to build"
	#sed -i 2s/.*/task-$desktop-desktop/ ./custom/hackeros/hackeros.list.chroot

	#package list
	cp custom/$DISTRIBNAME/$DISTRIBNAME.list.chroot ./config/package-lists/custom.list.chroot

done
