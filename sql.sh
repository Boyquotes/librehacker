#!/bin/sh

set -e

if [ ! -e '/proc/1' ]
then
    mount -t proc proc /proc
fi
#echo "mysql-server mysql-server/root_password password live" | debconf-set-selections
#echo "mysql-server mysql-server/root_password_again password live" | debconf-set-selections

#apt-get -y install mariadb-server
#echo 'replace'
#sed -i 's/3306/4951/' /etc/mysql/mariadb.conf.d/50-server.cnf
#mysql_status=`pgrep mysqld2 | head -1`
mysql_status=`ps aux |  grep mysqld2 | grep pid | awk {'print $2'}`
#ps aux | grep mysqld
echo $mysql_status
if [ -n "$mysql_status" ]
then
    echo 'KILL MYSQL'
    kill -9 $mysql_status
#    kill $(pgrep mysql)
#    kill $(pgrep mysqld)
#    kill $(pgrep mysqld_safe)
fi


#if [ -z $mysql_status ]
#then
echo 'start'
#service mysql start
#fi

#mysql -u root mysql < /etc/nv_pass.sql
echo 'CREATE DIRECTORY'
mkdir -p /var/run/mysqld2
mkdir -p /var/lib/mysql2
mkdir -p /var/log/mysql2
#root@ns365077:/# 
chown mysql /var/run/mysqld2
echo 'CHECK CONF'
#mysql2conf=$(grep mysql2 /etc/mysql/mariadb.conf.d/50-server.cnf)
#ls /etc/mysql/

if [ -z "$(cat /etc/mysql/mariadb.conf.d/50-server.cnf | grep mysqld2 )" ]
then
echo 'CONF'
cp /etc/mysql/mariadb.conf.d/50-server.cnf /etc/mysql/mariadb.conf.d/50-server.cnf.ori
echo '[mysqld2]
user            = mysql
pid-file        = /var/run/mysqld2/mysqld.pid
socket          = /var/run/mysqld2/mysqld.sock
port            = 33062
basedir         = /usr
datadir         = /var/lib/mysql2
tmpdir          = /tmp
lc-messages-dir = /usr/share/mysql
skip-external-locking
bind-address            = 127.0.0.1
general_log_file        = /var/log/mysql2/mysql.log
key_buffer_size         = 16M
max_allowed_packet      = 16M
thread_stack            = 192K
thread_cache_size       = 8
myisam-recover          = BACKUP
query_cache_limit       = 1M
query_cache_size        = 16M
log_error               = /var/log/mysql2/error.log
expire_logs_days        = 10
max_binlog_size         = 100M
' >> /etc/mysql/mariadb.conf.d/50-server.cnf

mysql_install_db --datadir=/var/lib/mysql2 --user=mysql
fi

#RUN
echo 'RUN'
cmd="nohup mysqld_multi --verbose --no-log start 2";
eval "${cmd}" &>/dev/null;
#mysqld_multi --verbose --no-log start 2


mysql -uroot --socket=/var/run/mysqld2/mysqld.sock -e 'SHOW DATABASES;'
mysql -uroot --socket=/var/run/mysqld2/mysqld.sock -e 'SELECT * FROM mysql.user;'
mysql -uroot --socket=/var/run/mysqld2/mysqld.sock -e 'SELECT plugin FROM mysql.user WHERE user="root" ;'

mysql -uroot --socket=/var/run/mysqld2/mysqld.sock -e 'FLUSH PRIVILEGES;'
mysql -uroot --socket=/var/run/mysqld2/mysqld.sock -e "UPDATE mysql.user SET plugin = 'mysql_native_password' WHERE user='root';"
mysql -uroot --socket=/var/run/mysqld2/mysqld.sock -e 'FLUSH PRIVILEGES;'
mysql -uroot --socket=/var/run/mysqld2/mysqld.sock -e 'FLUSH TABLES;'

mysql -uroot --socket=/var/run/mysqld2/mysqld.sock -e 'SELECT plugin FROM mysql.user WHERE user="root" ;'

#SED sur le fichier de conf
#sed -i 's/mysql2/mysql/g' /etc/mysql/mariadb.conf.d/50-server.cnf
#sed -i 's/33062/3306/' /etc/mysql/mariadb.conf.d/50-server.cnf
exit
mysql_status=`ps aux |  grep mysqld2 | grep pid | awk {'print $2'}`
#ps aux | grep mysqld
echo $mysql_status
if [ -n "$mysql_status" ]
then
    echo 'KILL MYSQL'
    kill -9 $mysql_status
#    kill $(pgrep mysql)
#    kill $(pgrep mysqld)
#    kill $(pgrep mysqld_safe)
fi

#MOVE DU REPETOIRE mysql2 vers mysql
rm -rf /var/lib/mysql
rm -rf /var/log/mysql
rm -rf /var/run/mysqld

mv /var/lib/mysql2 /var/lib/mysql
mv /var/log/mysql2 /var/log/mysql
mv /var/run/mysqld2 /var/run/mysqld

cp /etc/mysql/mariadb.conf.d/50-server.cnf /etc/mysql/mariadb.conf.d/50-server.cnf.bak
cp /etc/mysql/mariadb.conf.d/50-server.cnf.ori /etc/mysql/mariadb.conf.d/50-server.cnf



exit
#COMMAND A EXEC
#root@ns365077:/# cat /etc/nv_pass.sql 
mysql -uroot --socket=/var/run/mysqld2/mysqld.sock -e 'FLUSH PRIVILEGES;'
mysql -uroot --socket=/var/run/mysqld2/mysqld.sock -e "UPDATE mysql.user SET plugin = 'mysql_native_password' WHERE user='root';"
mysql -uroot --socket=/var/run/mysqld2/mysqld.sock -e 'FLUSH PRIVILEGES;'
mysql -uroot --socket=/var/run/mysqld2/mysqld.sock -e 'SHOW DATABASES;'
#SED sur le fichier de conf
#sed -i 's/mysql2/mysql/g' /etc/mysql/mariadb.conf.d/50-server.cnf
#sed -i 's/33062/3306/' /etc/mysql/mariadb.conf.d/50-server.cnf


#MOVE DU REPETOIRE mysql2 vers mysql
#mv /var/lib/mysql2 /var/lib/mysql
#mv /var/log/mysql2 /var/log/mysql
#mv /var/run/mysqld2 /var/run/mysqld

#cp /etc/mysql/mariadb.conf.d/50-server.cnf /etc/mysql/mariadb.conf.d/50-server.cnf.bak
#cp /etc/mysql/mariadb.conf.d/50-server.cnf.ori /etc/mysql/mariadb.conf.d/50-server.cnf
