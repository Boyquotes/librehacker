#!/bin/bash

## CONFIG USER
adduser hacker --gecos 'g,t,i,4,5' --disabled-password

echo 'hacker:live' | chpasswd

usermod -G cdrom,floppy,dip,video,netdev,plugdev,scanner,bluetooth,sudo,www-data,adm hacker

echo 'root:live' | chpasswd
## END CONFIG USER

## CONFIG SYSTEM
echo "librehacker" > /etc/hostname
echo "nameserver 80.67.169.12" > /etc/resolv.conf
echo "nameserver 192.168.1.1" >> /etc/resolv.conf
## END CONFIG SYSTEM

exit
ls
echo 'Build ISO'
./scripts/getLinuxLiveKit.sh
