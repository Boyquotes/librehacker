#!/bin/sh
echo "gsettingset"
dbus-uuidgen > /etc/machine-id
dbus-launch --exit-with-session gsettings set org.gnome.desktop.background picture-uri file:///usr/local/DISTRIBNAME/background.png
dbus-launch --exit-with-session gsettings set org.gnome.nautilus.icon-view thumbnail-size 32
dbus-launch --exit-with-session gsettings set org.gnome.nautilus.icon-view default-zoom-level small
