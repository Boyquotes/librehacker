#!/bin/bash
if [ "0$UID" -ne 0 ]; then
   echo "Only root can run $(basename $0) or use $(basename $0)"; exit 1
fi

rm -rf /media/nico/GLIM/multibootusb/librehacker-x86_64-100919/librehacker/01-core.hb
cp chroot/tmp/librehacker-data*/librehacker/01-core.hb /media/nico/GLIM/multibootusb/librehacker-x86_64-100919/librehacker/
sync
