#!/bin/bash
if [ "0$UID" -ne 0 ]; then
   echo "Only root can run $(basename $0) or use $(basename $0)"; exit 1
fi

umount -lf chroot/proc
umount chroot/proc
umount -lf chroot/sys
umount chroot/sys
umount -lf chroot/dev/pts
umount chroot/dev/pts
umount -lf chroot/dev
umount chroot/dev
umount -lf chroot/run
umount chroot/run

ls chroot/dev/
