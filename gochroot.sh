#!/bin/bash

if [ "0$UID" -ne 0 ]; then
   echo "Only root can run $(basename $0) or use $(basename $0)"; exit 1
fi

if [ ! -d "chroot/dev/" ]
then
    mkdir chroot/dev
fi
#check et creer proc
if [ ! -d "chroot/proc/" ]
then
    mkdir chroot/proc
fi
#check et creer proc
if [ ! -d "chroot/run/" ]
then
    mkdir chroot/run
fi
mount -o bind /run chroot/run

#check et creer tmp
if [ ! -d "chroot/tmp/" ]
then
    mkdir chroot/tmp
fi

mount -o bind /dev chroot/dev
#mount -t devfs devfs chroot/dev

# mount --bind /dev/pts chroot/dev/pts
# dont work mount -t devpts devpts chroot/dev/pts

#mount --bind   /etc/resolv.conf   /mnt/etc/resolv.conf

if [ ! -f "chroot/proc/cmdline" ]
then
    mount -t proc /proc chroot/proc
fi


cp getLinuxLiveKit.sh addUser.sh rebuild.sh chroot/tmp/

chroot chroot/
