#/bin/bash

/usr/bin/lb config --distribution buster --binary-images \
iso-hybrid --architectures amd64 --linux-flavours amd64 \
--linux-packages linux-image \
--archive-areas "main contrib non-free" --apt-indices false \
--memtest memtest86+ --security true --updates true --backports false \
--mirror-bootstrap http://ftp.fr.debian.org/debian/ \
--mirror-binary http://deb.debian.org/debian/ \
--bootappend-live "boot=live components timezone=Europe/Paris locales=en_US.UTF-8 keyboard-layouts=fr hostname=rescue username=user noprompt noeject autologin"
