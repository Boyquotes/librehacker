#!/bin/bash
apt install -y wget
history -c


#wget https://github.com/Tomas-M/linux-live/tarball/master --no-check-certificate -O master && tar xfv master
wget https://github.com/mintsoft/linux-live/tarball/uefi --no-check-certificate -O master && tar xfv master

if [ ! -e "./master" ]
then
    exit
fi

apt update && apt-get install --yes squashfs-tools xz-utils genisoimage aufs-dkms zip && apt-get clean

#cd T*
cd min*
sed -i 's/"linux/"librehacker/g' ./config
kernelVersion=`ls /boot/vmlinu* | cut -d'-' -f2-4`
echo $kernelVersion
sed -i 's/$(uname -r)/'$kernelVersion'/g' ./config

sed -i 's/\.sb/\.hb/g' ./config
sed -i "s/'sb'/'hb'/g" ./config
sed -i 's/BEXT=sb/BEXT=hb/g' ./config

sed -i 's/read DIR/DIR=$1/g' ./tools/isolinux.bin.update

cd ./bootfiles
../tools/isolinux.bin.update "/librehacker/boot"

cd ../
history -c
./build

cd /tmp
./gen_librehacker_iso.sh
./gen_librehacker_zip.sh
