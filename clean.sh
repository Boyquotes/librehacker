#!/bin/sh

if [ "0$UID" -ne 0 ]; then
   echo "Only root can run $(basename $0) or use $(basename $0)"; exit 1
fi

./umountChroot.sh
lb clean
rm -rf ./config/ ./cache/ ./.build/

